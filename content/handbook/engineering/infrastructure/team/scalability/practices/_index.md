---
aliases: /handbook/engineering/infrastructure/team/scalability/practices.html
title: "Scalability:Practices Team"
---

## Mission

We enable GitLab services to operate at production scale by providing paved roads for onboarding and maintaining features and services.



## Common Links
|                                |                                                                                                                                                                                                                                                                                                                                                                                                                                |
|--------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Workflow**                   | [Team workflow](/handbook/engineering/infrastructure/team/scalability/practices#how-we-work)|
| **GitLab.com**                 | `@gitlab-org/scalability/practices`                                                                                                                                                                                                                                                                                                                                                                                        |
| **Issue Trackers**             | [Scalability](https://gitlab.com/gitlab-com/gl-infra/scalability)                                                                                                                                                                                                                                                                                        |
| **Team Slack Channels**        | [#g_scalability-practices](https://gitlab.slack.com/archives/g_scalability-practices) - Team channel<br/> [#scalability_social](https://gitlab.slack.com/archives/g_scalability_social) - Group social channel                                                                                                                                                                                                         |
| **Information Slack Channels** | [#infrastructure-lounge](https://gitlab.slack.com/archives/infrastructure-lounge) (Infrastructure Group Channel), <br/>[#incident-management](https://gitlab.slack.com/archives/incident-management) (Incident Management),  <br/>[#alerts-general](https://gitlab.slack.com/archives/alerts-general) (SLO alerting) |


## Team Members

The following people are members of the Scalability:Practices team:

{{< team-by-manager-slug "kwanyangu" >}}

## Responsibilities

- [Runway](https://about.gitlab.com/direction/saas-platforms/scalability/runway/): Internal Platform as a Service for GitLab, enabling teams to deploy and run their services quickly and safely.
- [Production Readiness Review](/handbook/engineering/infrastructure/production/readiness/): A process that helps identify the reliability needs of a service, feature, or significant change to infrastructure for GitLab.com
- [Specific Counterparts Arrangements](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1203): Enabling specific stage group counterparts to self-serve on SRE support
- Redis
- Sidekiq
- Participation in SRE on-call rotation

## How We Work

### Communication

- **Slack Updates:** Regular status updates in [#g_scalability-practices](https://gitlab.slack.com/archives/g_scalability-practices) Slack channel with no strict update frequency
- **Scheduled Calls:** Working async is our preferred and default way of communication. We also have a weekly group call for synchronous discussions on any open questions that async discussions did not cover
